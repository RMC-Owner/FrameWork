package pw.cmdev.framework;

import org.bukkit.entity.Player;

import java.util.logging.Level;

/**
 * @author Mitchell Cook
 * @date 26/07/16
 **/

public interface IUtils {

    void log(Level level, String string);
    void color(Player player, String string);
    void prefix(Player player, String prefix, String string);
    void nullPerms(Player player);
    String translate(String string);
    String pTranslate(String string);
    boolean hasGPerm(Player player, String gPerms);

}
